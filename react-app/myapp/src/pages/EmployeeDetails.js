import React, { useEffect, useState } from "react";
import axios from "axios";
import Employee from "../components/Employee";
import { URL } from "../../src/config";
function EmployeeDetails() {
  const [employees, setEmployees] = useState([]);
  const getEmployeesFromServer = () => {
    const url = `${URL}/employee/getAllEmployees`;
    axios.get(url).then((res) => {
      const result = res.data;
      if (result.status == "success") setEmployees(result.data);
      else console.log("data not recieved");
    });
  };
  useEffect(() => {
    getEmployeesFromServer();
  }, []);

  return (
    <div className="container">
      <table
        className="table table-hover table-dark "
        style={{ color:"orange",marginLeft: "50px", borderStyle: "solid" }}
      >
        <tr>
          <td>Empid</td>
          <td>Name</td>
          <td>Email</td>
        </tr>
        {employees.map((e) => {
          return <Employee employee={e} />;
        })}
      </table>
    </div>
  );
}

export default EmployeeDetails;
