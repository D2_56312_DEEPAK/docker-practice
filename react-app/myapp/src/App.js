import logo from './logo.svg';
import './App.css';
import {Router,BrowserRouter,Link,Route,Routes} from "react-router-dom"
import EmployeeDetails from './pages/EmployeeDetails';
function App() {

  return (
    <div>
      <BrowserRouter>
        <Routes>
         <Route path="/" element={<EmployeeDetails/>} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
