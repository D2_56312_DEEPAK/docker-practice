import React from 'react'

function Employee(props) {
    const {employee}=props
    return (
       <tr className="table table-hover" style={{color:"blue",borderStyle:"solid"}}>
           <td>{employee.id}</td><td>{employee.name}</td><td>{employee.email}</td>
       </tr>
    )
}

export default Employee
