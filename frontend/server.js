const express = require("express");
const empRouter = require("./routes/emp");
const cors = require("cors");

const app = express();
app.use(cors("*"));
app.use(express.json());
app.use("/employee", empRouter);
app.listen(4000, () => {
  console.log("server started on 4000");
});
