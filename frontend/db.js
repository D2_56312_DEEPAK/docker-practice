const mysql = require("mysql2");
const fetchConnection = () => {
  const connection = mysql.createConnection({
    database: "testdb",
    host: "database",
    user: "root",
    password: "manager",
  });
  connection.connect();

  return connection;
};

module.exports = {
  fetchConnection,
};
