const express=require("express")
const router=express.Router()
const db=require("../db")
const utils=require("../utils")

router.post("/addEmployee",(req,res)=>{
    const {email,name,password}=req.body
    const sql=`insert into emp values(0,'${name}','${email}','${password}')`
    const connection=db.fetchConnection()
    connection.query(sql,(error,data)=>{
        connection.end()
        if(error)
        res.send(utils.createResult(error))
        if(data)
        res.send(utils.createResult(null,data))

    })
    
    
    
})
router.get("/getAllEmployees",(req,res)=>{
    const connection =db.fetchConnection()
    const sql=`select * from emp`
    connection.query(sql,(error,data)=>{
        connection.end()
        if(error)
        res.send(utils.createResult(error))
        if(data)
        res.send(utils.createResult(null,data))

    })
})
router.delete("/remove/:id",(req,res)=>{
    const {id}=req.params
    const connection =db.fetchConnection()
    const sql=`delete from emp where id =${id}`
    connection.query(sql,(error,data)=>{
        connection.end()
        if(error)
        res.send(utils.createResult(error))
        if(data)
        res.send(utils.createResult(null,data))

    })
})

module.exports=router;